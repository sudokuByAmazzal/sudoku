#include "Res\Header.h"
int WINAPI WinMain (HINSTANCE hThisInstance,HINSTANCE hPrevInstance,LPSTR lpszArgument,int nFunsterStil)
{
    if(FindWindow(szClassName,NULL)!=0)
    return 0;
    if(lpszArgument[0]!='\0')//there is a file to open.
    {
        File=fopen(lpszArgument,"rb");
        if(File==NULL)
        {
            return 0;
        }
    }else{
                    HKEY hKey;
                    GetModuleFileName(GetModuleHandle(NULL),Filepath,MAX_PATH);
                    char Path[strlen(Filepath)+30];
                    if (RegCreateKey(HKEY_CURRENT_USER,"Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\FileExts\\.SOK\\OpenWithList",&hKey)!=ERROR_SUCCESS)
                    {
                        MessageBox(hwnd,"An error accured When Saving Data to register",0,0);
                    }else
                    {
                        if (RegSetValueExA(hKey,"a",(DWORD)NULL, (DWORD)REG_SZ,(const BYTE*)"HaSoduko.exe", strlen("HaSoduko.exe"))!=ERROR_SUCCESS || RegSetValueEx(hKey,"MRUList", NULL, REG_SZ, (LPBYTE)"a", strlen("a"))!=ERROR_SUCCESS)
                        {
                            MessageBox(hwnd,"An error accured When Saving Data to register",0,0);
                        }
                    }

                    RegCloseKey(hKey);
                    if (RegCreateKey(HKEY_CURRENT_USER,"Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\FileExts\\.SOK\\OpenWithProgids",&hKey)!=ERROR_SUCCESS)
                    {
                        MessageBox(hwnd,"An error accured When Saving Data to register",0,0);
                    }else
                    {
                        if (RegSetValueEx(hKey,"SOK_auto_file",(DWORD)NULL, REG_NONE,(const BYTE*)"",strlen(""))!=ERROR_SUCCESS )
                        {
                            MessageBox(hwnd,"An error accured When Saving Data to register",0,0);
                        }
                    }

                    RegCloseKey(hKey);
                    if (RegCreateKey(HKEY_CLASSES_ROOT,"SOK_auto_file",&hKey)!=ERROR_SUCCESS)
                    {
                        MessageBox(hwnd,"An error accured When Saving Data to register",0,0);
                    }else
                    {
                        if (RegSetValueExA(hKey,"",(DWORD)NULL, REG_SZ,(const BYTE*)"HaSoduko Game File",strlen("HaSoduko Game File"))!=ERROR_SUCCESS)
                        {
                            MessageBox(hwnd,"An error accured When Saving Data to register",0,0);
                        }
                        if(RegCreateKey(HKEY_CLASSES_ROOT,"SOK_auto_file\\DefaultIcon",&hKey)!=ERROR_SUCCESS)
                        {
                            MessageBox(hwnd,"An error accured When Saving Data to register",0,0);
                        }else
                        {
                                    wsprintf(Path,"\"%s\",1",Filepath);
                                    if (RegSetValueEx(hKey,"",(DWORD)NULL, REG_SZ,(const BYTE*)Path,strlen(Path))!=ERROR_SUCCESS)
                                    {
                                        MessageBox(hwnd,"An error accured When Saving Data to register",0,0);
                                    }
                        }
                        if(RegCreateKey(HKEY_CLASSES_ROOT,"SOK_auto_file\\shell\\open",&hKey)!=ERROR_SUCCESS)
                        {
                            MessageBox(hwnd,"An error accured When Saving Data to register",0,0);
                        }else
                        {
                                if (RegSetValueEx(hKey,"",(DWORD)NULL, REG_SZ,(const BYTE*)"Open This Game",strlen("Open This Game"))!=ERROR_SUCCESS)
                                {
                                    MessageBox(hwnd,"An error accured When Saving Data to register",0,0);
                                }
                                if(RegCreateKey(HKEY_CLASSES_ROOT,"SOK_auto_file\\shell\\open\\command",&hKey)!=ERROR_SUCCESS)
                                {
                                    MessageBox(hwnd,"An error accured When Saving Data to register",0,0);
                                }else
                                {
                                    wsprintf(Path,"\"%s\" %c%d",Filepath,'%',1);
                                    if (RegSetValueEx(hKey,"",(DWORD)0, REG_SZ,(const BYTE*)Path,strlen(Path))!=ERROR_SUCCESS)
                                    {
                                        MessageBox(hwnd,"An error accured When Saving Data to register",0,0);
                                    }
                                }
                        }
                    }
                    RegCloseKey(hKey);
    }

    MSG messages;
    WNDCLASSEX wincl;
    wincl.hInstance = hThisInstance;
    wincl.lpszClassName = szClassName;
    wincl.lpfnWndProc = WindowProcedure;
    wincl.style = CS_BYTEALIGNCLIENT | CS_BYTEALIGNWINDOW| CS_DBLCLKS;
    wincl.cbSize = sizeof (WNDCLASSEX);
    wincl.hIcon = LoadIcon (GetModuleHandle(NULL),  MAKEINTRESOURCE(MyIcon));
    wincl.hIconSm = LoadIcon (GetModuleHandle(NULL),  MAKEINTRESOURCE(MyIcon));
    wincl.hCursor = LoadCursor (NULL, IDC_ARROW);
    wincl.lpszMenuName = MAKEINTRESOURCE(MyMenu);
    wincl.cbClsExtra = 0;
    wincl.cbWndExtra = 0;
    wincl.hbrBackground = (HBRUSH)CreateSolidBrush(0x00333332) ;
    if (!RegisterClassEx (&wincl))
        return 0;
        DWORD dwWidth = GetSystemMetrics(SM_CXFULLSCREEN);
        DWORD dwHeight = GetSystemMetrics(SM_CYFULLSCREEN);
    hwnd = CreateWindowEx (WS_EX_ACCEPTFILES,szClassName, szClassName,WS_POPUPWINDOW
                           ,0,0,dwWidth,dwHeight+20,0,(HMENU)NULL,hThisInstance,NULL);
    ShowWindow (hwnd,SW_MAXIMIZE);
    while (GetMessage (&messages, NULL, 0, 0))
    {
        TranslateMessage(&messages);
        DispatchMessage(&messages);
    }
    return messages.wParam;
}
BOOL CALLBACK DOWNLoadDlg (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch(message)
    {
        case WM_INITDIALOG:
        			BtmImg[0] = LoadBitmap(GetModuleHandle(NULL), MAKEINTRESOURCE(UPDATE));
                    BtmImg[1] = LoadBitmap(GetModuleHandle(NULL), MAKEINTRESOURCE(UPDATE2));
                   /* int nResult=WSAStartup(MAKEWORD(2,2),&WSAdata);
                    if(nResult!=0)
                    {
                        MessageBox(hwnd,"Winsock initialization failed","Critical Error",MB_ICONERROR);
                    }
                    SOCKET Sock=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
                    if(Sock==INVALID_SOCKET)
                    {
                        MessageBox(hwnd,"Socket creation failed","Critical Error",MB_ICONERROR);
                    }
                    nResult=WSAAsyncSelect(Sock,hwnd,WM_SOCKET,(FD_CLOSE|FD_READ|FD_WRITE|FD_CONNECT));
                    if(nResult)
                    {
                        MessageBox(hwnd,"WSAAsyncSelect failed","Critical Error",MB_ICONERROR);
                    }
                    if((host=gethostbyname("localhost"))==NULL)
                    {
                        MessageBox(hwnd,"Unable to resolve host name","Critical Error",MB_ICONERROR);
                    }
                    Saddr.sin_family=AF_INET;
                    Saddr.sin_port=ntohs(8080);
                    Saddr.sin_addr.s_addr=*((unsigned long*)host->h_addr);*/
        return TRUE;
        case WM_PAINT:
		{
			BITMAP bm;
			PAINTSTRUCT ps;
			HDC hdc = BeginPaint(hwnd, &ps);
			HDC hdcMem = CreateCompatibleDC(hdc);
			HBITMAP hbmOld = (HBITMAP)SelectObject(hdcMem, BtmImg[0]);
			GetObject(BtmImg[0], sizeof(bm), &bm);
			BitBlt(hdc,20,10, bm.bmWidth, bm.bmHeight, hdcMem, 0,0, SRCCOPY);

            hbmOld = (HBITMAP)SelectObject(hdcMem, BtmImg[1]);
			GetObject(BtmImg[1], sizeof(bm), &bm);
			BitBlt(hdc,0,310, bm.bmWidth, bm.bmHeight, hdcMem, 0,0, SRCCOPY);

			SelectObject(hdcMem, hbmOld);
			DeleteDC(hdcMem);
			EndPaint(hwnd, &ps);
		}
        break;
        case WM_CTLCOLORDLG:
            return (LONG)CreateSolidBrush(RGB(255,255,255));
        break;
       /* case WM_SOCKET:
            if(WSAGETSELECTERROR(lParam))
			{
				MessageBox(hwnd,
					"Connection to server failed",
					"Error",
					MB_OK|MB_ICONERROR);
				break;
			}
            switch(WSAGETSELECTEVENT(lParam))
            {
                case FD_CONNECT:
                    nResult=connect(Sock,(SOCKADDR*)(&Saddr),sizeof(Saddr));
                    if ( nResult == SOCKET_ERROR)
                    {
                        closesocket (Sock);
                        printf("Unable to connect to server: %ld\n", WSAGetLastError());
                        WSACleanup();
                    }
                break;
                case FD_CLOSE:
                    closesocket(Sock);
                break;
                case FD_READ:
                    memset(ReveivedText,0,999);
                    if(recv(Sock,ReveivedText,100,0)>0)
                    MessageBox(hwnd,ReveivedText,"",0);
                break;
                case FD_WRITE:
                    if(send(Sock,TextInfoToSend,strlen(TextInfoToSend),0)!= SOCKET_ERROR)
                        shutdown(Sock, SD_SEND);
                break;
            }
        break;*/
        case WM_CTLCOLORSTATIC:
            SetTextColor((HDC)wParam,RGB(0,0,0));
            SetBkMode((HDC)wParam,1);
            return (LONG)CreateSolidBrush(RGB(255,255,255));
        break;
        case WM_COMMAND:
            switch(LOWORD(wParam))
            {
                case IDC_BUTTONDOWNLoad:

                break;
                case IDC_BUTTONCANCEL:
                WSACleanup();
                EndDialog(hwnd,IDC_BUTTONCANCEL);
                break;
            }
        break;
        default:
            return FALSE;
    }
    return TRUE;
}


BOOL CALLBACK LVDlg (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch(message)
    {
        case WM_INITDIALOG:
                    u=0;
        			BtmImg[0] = LoadBitmap(GetModuleHandle(NULL), MAKEINTRESOURCE(Sdk1));
                    BtmImg[1] = LoadBitmap(GetModuleHandle(NULL), MAKEINTRESOURCE(Sdk2));
                    BtmImg[2] = LoadBitmap(GetModuleHandle(NULL), MAKEINTRESOURCE(Sdk3));
            SetFocus(hwnd);
            SendMessage(GetDlgItem(hwnd,IDC_LV1),BM_SETCHECK,BST_CHECKED,0);
        return TRUE;
        case WM_PAINT:
		{
			BITMAP bm;
			PAINTSTRUCT ps;
			HDC hdc = BeginPaint(hwnd, &ps);
			HDC hdcMem = CreateCompatibleDC(hdc);
			HBITMAP hbmOld = (HBITMAP)SelectObject(hdcMem, BtmImg[u]);
			GetObject(BtmImg[u], sizeof(bm), &bm);
			BitBlt(hdc,132,7, bm.bmWidth, bm.bmHeight, hdcMem, 0, 0, SRCCOPY);
			SelectObject(hdcMem, hbmOld);
			DeleteDC(hdcMem);
			EndPaint(hwnd, &ps);
		}
        break;
        case WM_COMMAND:
        switch(LOWORD(wParam))
        {
            case IDC_LV1:
                u=2;
                Level=5;
                InvalidateRect(hwnd,NULL,TRUE);
            break;
            case IDC_LV2:
                u=1;
                Level=4;
                 InvalidateRect(hwnd,NULL,TRUE);
            break;
            case IDC_LV3:
                u=0;
                Level=3;
                 InvalidateRect(hwnd,NULL,TRUE);
            break;
            case IDC_BUTTONOK:
                DeleteObject(BtmImg[0]);
                DeleteObject(BtmImg[1]);
                DeleteObject(BtmImg[2]);
                EndDialog(hwnd,Level);
            break;
        }
        break;
        default:
            return FALSE;
    }
    return TRUE;
}
BOOL CALLBACK AboutDlg (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch(message)
    {
        case WM_INITDIALOG:
        			BtmImg2 = LoadBitmap(GetModuleHandle(NULL), MAKEINTRESOURCE(MyPic));
			if(BtmImg2 == NULL)
				MessageBox(hwnd, "Could not load An image!", "Error", MB_OK | MB_ICONEXCLAMATION);
            SetFocus(hwnd);
            SetWindowText(GetDlgItem(hwnd,265),"Programed In C by Amazzal Elhabib(The Inventor Habib),\n\x0A About the game:\nSudoku  is a logic-based,combinatorial number-placement puzzle. The objective is to fill a 9�9 grid with digits so that each column, each row, and each of the nine 3�3 sub-grids that compose the grid (also called boxes, blocks, regions, or sub-squares) contains all of the digits from 1 to 9. The puzzle setter provides a partially completed grid, which typically has a unique solution.Learn More:http://en.wikipedia.org/wiki/Sudoku");
        return TRUE;
        case WM_CLOSE:
            EndDialog(hwnd,0);
        break;

        case WM_PAINT:
		{
			BITMAP bm;
			PAINTSTRUCT ps;

			HDC hdc = BeginPaint(hwnd, &ps);

			HDC hdcMem = CreateCompatibleDC(hdc);
			HBITMAP hbmOld = (HBITMAP)SelectObject(hdcMem, BtmImg2);

			GetObject(BtmImg2, sizeof(bm), &bm);

			BitBlt(hdc,0,0,422,256, hdcMem,0,0, SRCCOPY);

			SelectObject(hdcMem, hbmOld);
			DeleteDC(hdcMem);

			EndPaint(hwnd, &ps);
		}
        break;
        case WM_COMMAND:
            switch(LOWORD(wParam))
            {
                case IDC_OK:
                DeleteObject(BtmImg2);
                EndDialog(hwnd,IDC_OK);
                break;
            }
        break;
        default:
            return FALSE;
    }
    return TRUE;
}

LRESULT CALLBACK WindowProcedure (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
        case WM_DESTROY:
            DeleteObject(BtmImg);
            PostQuitMessage (0);
        break;
        case WM_COMMAND:
            switch(LOWORD(wParam))
            {
                case Mstr:
                {
                        if(GameStart)
                        {
                            SendMessage(hStatus,SB_SETTEXT,0,(LPARAM)"Game Started");
                            char T[4];
                            x=0;
                            while(x<9)
                            {
                                       y=0;
                                       while(y<9)
                                       {
                                           if(SDOK_NUMBERS2[x][y]==0)
                                           {
                                                 GetWindowText(Hwnd[x][y],T,2);
                                                 SDOK_NUMBERS2[x][y]=Char2Int(T[0]);
                                                 if(SDOK_NUMBERS2[x][y]!=0)
                                                 {

                                                     if(IsValidNumber(x,y,SDOK_NUMBERS2[x][y]))
                                                     {
                                                         InvalidateRect(hwnd,NULL,TRUE);
                                                        EnableWindow(Hwnd[x][y],FALSE);
                                                        if(CheckCom())
                                                        {
                                                            MessageBox(hwnd,"Smart Man ,Try again Go to next Level.","Good Work",0);
                                                            SetZero(SDOK_NUMBERS2);
                                                            SetZero(SDOK_NUMBERS3);
                                                            DisableAll();
															InvalidateRect(hwnd,NULL,TRUE);
                                                            return 0;
                                                        }

                                                     }else
                                                     {
                                                         if(SendMessage(ToolbarHwnd,TB_ISBUTTONCHECKED,IMG_BITMAPDeletUnvalidone,0)!=0)
                                                         {
                                                             SDOK_NUMBERS2[x][y]=0;
                                                             SetWindowText(Hwnd[x][y],"");
                                                             MessageBox(hwnd,"You have entered An unvalid Number,But It has corrected because you selected this feature","Prompt",0);
                                                         }else
                                                         {
                                                             MessageBox(hwnd,"You have entered An unvalid Number,You have to start again","Error",0);
                                                            SetZero(SDOK_NUMBERS2);
                                                            SetZero(SDOK_NUMBERS3);
                                                            DisableAll();
                                                             InvalidateRect(hwnd,NULL,TRUE);
                                                             return 0;
                                                         }
                                                     }

                                                 }else
                                                 {
                                                    EnableWindow(Hwnd[x][y],TRUE);
                                                 }
                                           }else
                                           {
                                                if(IsWindowEnabled(Hwnd[x][y])!=FALSE)
                                                {
                                                    itoa(SDOK_NUMBERS2[x][y],T,10);
                                                    SetWindowText(Hwnd[x][y],T);
                                                    EnableWindow(Hwnd[x][y],FALSE);
                                                }
                                           }
                                            y++;
                                       }
                                       x++;
                            }
                                if(SendMessage(ToolbarHwnd,TB_ISBUTTONENABLED,BTN_CHECK,TRUE)!=TRUE)
                                {
                                    SendMessage(ToolbarHwnd,TB_ENABLEBUTTON,BTN_CHECK,TRUE);
                                    EnableMenuItem(GetMenu(hwnd),MITEM_CHECK,MF_ENABLED );
                                }
                                if(SendMessage(ToolbarHwnd,TB_ISBUTTONENABLED,BTN_ADDone,TRUE)!=TRUE && (!CheckCom() && HelpingOutCounter<=4))
                                {
                                    SendMessage(ToolbarHwnd,TB_ENABLEBUTTON,BTN_ADDone,TRUE);
                                    EnableMenuItem(GetMenu(hwnd),MITEM_ADDone,MF_ENABLED);
                                }
                                if(SendMessage(ToolbarHwnd,TB_ISBUTTONENABLED,BTN_ReSet,TRUE)!=TRUE)
                                {
                                    SendMessage(ToolbarHwnd,TB_ENABLEBUTTON,BTN_ReSet,TRUE);
                                    EnableMenuItem(GetMenu(hwnd),MITEM_ReSet,MF_ENABLED);
                                }
                               if(SendMessage(ToolbarHwnd,TB_ISBUTTONENABLED,MITEM_SAVEF,TRUE)!=TRUE)
                                {
                                    SendMessage(ToolbarHwnd,TB_ENABLEBUTTON,MITEM_SAVEF,TRUE);
                                    EnableMenuItem(GetMenu(hwnd),MITEM_SAVEF,MF_ENABLED);
                                    EnableMenuItem(GetMenu(hwnd),MITEM_SAVEHtml,MF_ENABLED);
                                }
                                if(SendMessage(ToolbarHwnd,TB_ISBUTTONENABLED,MITEM_Close,TRUE)!=TRUE)
                                {
                                    SendMessage(ToolbarHwnd,TB_ENABLEBUTTON,MITEM_Close,TRUE);
                                    EnableMenuItem(GetMenu(hwnd),MITEM_Close,MF_ENABLED);
                                }

                    }else{
                            SendMessage(hStatus,SB_SETTEXT,0,(LPARAM)"Game Not Started");
                            DisableAll(FALSE);
                            if(SendMessage(ToolbarHwnd,TB_ISBUTTONENABLED,MITEM_CHECK,(LPARAM)TRUE)!=FALSE)
                            {
                                 SendMessage(ToolbarHwnd,TB_ENABLEBUTTON,MITEM_CHECK,(LPARAM)FALSE);
                                EnableMenuItem(GetMenu(hwnd),MITEM_CHECK,MF_DISABLED | MF_GRAYED);
                            }
                            if(SendMessage(ToolbarHwnd,TB_ISBUTTONENABLED,MITEM_ADDone,(LPARAM)TRUE)!=FALSE)
                            {
                                SendMessage(ToolbarHwnd,TB_ENABLEBUTTON,MITEM_ADDone,(LPARAM)FALSE);
                                EnableMenuItem(GetMenu(hwnd),MITEM_ADDone,MF_DISABLED | MF_GRAYED);
                            }
                            if(SendMessage(ToolbarHwnd,TB_ISBUTTONENABLED,BTN_ReSet,(LPARAM)TRUE)!=FALSE)
                            {
                                SendMessage(ToolbarHwnd,TB_ENABLEBUTTON,BTN_ReSet,(LPARAM)FALSE);
                                EnableMenuItem(GetMenu(hwnd),MITEM_ReSet,MF_DISABLED|MF_GRAYED);
                            }
                            if(SendMessage(ToolbarHwnd,TB_ISBUTTONENABLED,MITEM_SAVEF,(LPARAM)TRUE)!=FALSE)
                            {
                                SendMessage(ToolbarHwnd,TB_ENABLEBUTTON,MITEM_SAVEF,(LPARAM)FALSE);
                                EnableMenuItem(GetMenu(hwnd),MITEM_SAVEF,MF_DISABLED|MF_GRAYED);
                                EnableMenuItem(GetMenu(hwnd),MITEM_SAVEHtml,MF_DISABLED|MF_GRAYED);
                            }
                             if(SendMessage(ToolbarHwnd,TB_ISBUTTONENABLED,MITEM_Close,(LPARAM)TRUE)!=FALSE)
                            {
                                SendMessage(ToolbarHwnd,TB_ENABLEBUTTON,MITEM_Close,(LPARAM)FALSE);
                                EnableMenuItem(GetMenu(hwnd),MITEM_Close,MF_DISABLED|MF_GRAYED);
                            }
                    }

                }
                break;
                case BTN_CHECK:
                        if(!CheckCom())
                        {
                            x=Time;
                            MessageBox(hwnd,"Every thing is OK,But you still hasn't finished yet","Prompt",0);
                            Time=x;
                        }

                break;
                case BTN_CLOSE:
                   PostQuitMessage(0);
                break;
                case MITEM_Help:
                    DialogBox(GetModuleHandle(NULL),MAKEINTRESOURCE(101),hwnd,AboutDlg);
                break;
                case MITEM_SAVEF:
                    SaveGame();
                break;

                case MITEM_LOADF:
                    LoadGame();
                    if(hThread!=NULL)TerminateThread(hThread,dwThreadId);
                   if ((hThread=CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)&ThreadProc,(LPVOID)5,0,&dwThreadId))== NULL)
                    {
                        MessageBox(hwnd,"Error Creating Thread",0,0);
                    }
                     InvalidateRect(hwnd,NULL,TRUE);
                break;
                case MITEM_SAVEHtml:
                    SaveHtml();
                break;
                case BTN_NewGame:
                {
                    Time=0;
                    GameStart=1;
                    Level=5;
                    DialogBox(GetModuleHandle(NULL),MAKEINTRESOURCE(LevelDlg),hwnd,LVDlg);
                    HelpingOutCounter=0;
                    SetNums();
                    SetKnownNumbers();
                    SendMessage(hwnd,WM_COMMAND,(WPARAM)Mstr,(LPARAM)0);
                    if(hThread!=NULL)TerminateThread(hThread,dwThreadId);
                    if ((hThread=CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)&ThreadProc,(LPVOID)5,0,&dwThreadId))== NULL)
                    {
                        MessageBox(hwnd,"Error Creating Thread",0,0);
                    }
                    InvalidateRect(hwnd,NULL,TRUE);
                }
                break;
                case 5:
                    PostMessage(hwnd,WM_COMMAND,(WPARAM)Mstr,(LPARAM)0);
                break;
                case BTN_ADDone:
                HelpingOutCounter++;
                if(HelpingOutCounter<=5)
                {
                    InvalidateRect(hwnd,NULL,TRUE);
                    if(AddNextOne()==0)
                    {
                         PostMessage(hwnd,WM_COMMAND,(WPARAM)Mstr,(LPARAM)0);
                    }
                }else
                {
                                SendMessage(ToolbarHwnd,TB_ENABLEBUTTON,(WPARAM)BTN_ADDone,(LPARAM)FALSE);
                                EnableMenuItem(GetMenu(hwnd),MITEM_ADDone,MF_DISABLED|MF_GRAYED);
                }
                break;
                case MITEM_Close:
                    if(MessageBox(hwnd,"Do you want to save this Game:","Prompt",MB_YESNO)==6)
                    {
                        SaveGame();
                    }
                    SetZero(SDOK_NUMBERS2);
                    SetZero(SDOK_NUMBERS3);
                    DisableAll();
                     InvalidateRect(hwnd,NULL,TRUE);
                break;
                case BTN_ReSet:
                {
                    Time=0;
                    HelpingOutCounter=0;
                     SetZero(NULL);
                    SetCurValue(SDOK_NUMBERS3);
                     InvalidateRect(hwnd,NULL,TRUE);
                }
                break;
                case IMG_BITMAPDeletUnvalidone:
                {
                    SendMessage(ToolbarHwnd,TB_CHECKBUTTON,(WPARAM)IMG_BITMAPDeletUnvalidone,(LPARAM)TRUE);
                   if( CheckMenuItem(GetMenu(hwnd),IMG_BITMAPDeletUnvalidone,MF_CHECKED)==MF_CHECKED)
                   {
                       SendMessage(ToolbarHwnd,TB_CHECKBUTTON,(WPARAM)IMG_BITMAPDeletUnvalidone,(LPARAM)FALSE);
                        CheckMenuItem(GetMenu(hwnd),IMG_BITMAPDeletUnvalidone,MF_UNCHECKED);
                   }
                }
                break;
                case MITEM_Update:
                            DialogBox(GetModuleHandle(NULL),MAKEINTRESOURCE(DownLoadDlg),hwnd,DOWNLoadDlg);
                break;
                case 563:
                    ShowWindow(hwnd,SW_SHOWMINIMIZED);
                break;
            }
        break;
        case WM_CLOSE:
            PostQuitMessage(0);
        break;
        case WM_CREATE:
        {
            int x,y;
            x=0;
            int j=100,j2=12;
            while(x<9)
            {
                y=0;
                j+=28;
                j2=100;
                if(x%3==0)j+=5;
                while(y<9)
                {
                    j2+=(j2==0?0:28);
                    if(y%3==0)j2+=5;
                    Hwnd[x][y]=CreateWindow("EDIT","",SS_CENTERIMAGE|SS_CENTER|WS_CHILD|WS_DISABLED|WS_VISIBLE|ES_NUMBER|ES_CENTER|WS_BORDER,j2,j,28,28,hwnd,(HMENU)5,GetModuleHandle(NULL),0);
                    y++;
                }
                x++;
            }
            RECT WinRect;
            GetWindowRect(hwnd,&WinRect);
            ToolbarHwnd=CreateWindowEx(0, TOOLBARCLASSNAME,0,WS_CHILD | WS_VISIBLE|WS_BORDER, 0, 0,0,0,hwnd, (HMENU)NULL,GetModuleHandle(NULL), NULL);
            SendMessage(ToolbarHwnd, TB_BUTTONSTRUCTSIZE, (WPARAM)sizeof(TBBUTTON),(LPARAM)0);
            TBADDBITMAP tbab;
            tbab.hInst =NULL;
            tbab.nID = (UINT)LoadBitmap(GetModuleHandle(NULL), MAKEINTRESOURCE(KH));
            SendMessage(ToolbarHwnd, TB_ADDBITMAP,(WPARAM)9, (LPARAM)&tbab);
            TBBUTTON tbb[11];
            tbb[0].iBitmap = 0;
            tbb[0].fsState = TBSTATE_ENABLED;
            tbb[0].fsStyle = TBSTYLE_BUTTON;
            tbb[0].idCommand = BTN_CLOSE;
            tbb[0].dwData = 0;
            tbb[0].iString = 0;

            tbb[1].iBitmap = 1;
            tbb[1].fsState = TBSTATE_ENABLED;
            tbb[1].fsStyle = TBSTYLE_BUTTON;
            tbb[1].idCommand = MITEM_LOADF;
            tbb[1].dwData = 0;
            tbb[1].iString = 0;

            tbb[2].iBitmap = 2;
            tbb[2].fsState =0;
            tbb[2].fsStyle = TBSTYLE_BUTTON;
            tbb[2].idCommand = MITEM_SAVEF;
            tbb[2].dwData = 0;
            tbb[2].iString = 0;

            tbb[3].iBitmap = 3;
            tbb[3].fsState = TBSTATE_ENABLED;
            tbb[3].fsStyle = TBSTYLE_BUTTON;
            tbb[3].idCommand = BTN_NewGame;
            tbb[3].dwData = 0;
            tbb[3].iString = 0;

            tbb[4].iBitmap = 4;
            tbb[4].fsState = 0;
            tbb[4].fsStyle = TBSTYLE_BUTTON;
            tbb[4].idCommand = BTN_ReSet;
            tbb[4].dwData = 0;
            tbb[4].iString = 0;

            tbb[5].iBitmap = 6;
            tbb[5].fsState = 0;
            tbb[5].fsStyle = TBSTYLE_BUTTON;
            tbb[5].idCommand = BTN_ADDone;
            tbb[5].dwData = 0;
            tbb[5].iString = 0;

            tbb[6].iBitmap = 7;
            tbb[6].fsState = 0;
            tbb[6].fsStyle = TBSTYLE_BUTTON;
            tbb[6].idCommand = BTN_CHECK;
            tbb[6].dwData = 0;
            tbb[6].iString = 0;



            tbb[7].iBitmap = 5;
            tbb[7].fsState = 0;
            tbb[7].fsStyle = TBSTYLE_BUTTON;
            tbb[7].idCommand = MITEM_Close;
            tbb[7].dwData = 0;
            tbb[7].iString = 0;

            tbb[8].iBitmap = 8;
            tbb[8].fsState = TBSTATE_ENABLED;
            tbb[8].fsStyle = TBSTYLE_BUTTON;
            tbb[8].idCommand = MITEM_Help;
            tbb[8].dwData = 0;
            tbb[8].iString = 0;

            tbb[9].iBitmap = 12;
            tbb[9].fsState = TBSTATE_ENABLED;
            tbb[9].fsStyle = 0x00000001;
            tbb[9].idCommand = 0;
            tbb[9].dwData = 0;
            tbb[9].iString =0;

            tbb[10].iBitmap = 9;
            tbb[10].fsState = TBSTATE_ENABLED;
            tbb[10].fsStyle = TBSTYLE_CHECK;
            tbb[10].idCommand = IMG_BITMAPDeletUnvalidone;
            tbb[10].dwData = 0;
            tbb[10].iString =0;
            SendMessage(ToolbarHwnd, TB_ADDBUTTONS,(WPARAM)11, (LPARAM)&tbb);

            hStatus=CreateWindowEx(0,STATUSCLASSNAME,NULL,WS_CHILD |WS_BORDER |WS_VISIBLE, 0, 0, 0, 0,hwnd,(HMENU)0,NULL,NULL);
            GameStart=0;
            SetMenuItemBitmaps(GetMenu(hwnd),MITEM_NewGame,MF_BYCOMMAND, LoadBitmap(GetModuleHandle(NULL), MAKEINTRESOURCE(IMG_BITMAP2)),0);
            SetMenuItemBitmaps(GetMenu(hwnd),MITEM_LOADF,MF_BYCOMMAND, LoadBitmap(GetModuleHandle(NULL), MAKEINTRESOURCE(IMG_BITMAPLOAD)),0);
            SetMenuItemBitmaps(GetMenu(hwnd),MITEM_SAVEF,MF_BYCOMMAND, LoadBitmap(GetModuleHandle(NULL), MAKEINTRESOURCE(IMG_BITMAPSAVE)),0);
            SetMenuItemBitmaps(GetMenu(hwnd),MITEM_Exit,MF_BYCOMMAND, LoadBitmap(GetModuleHandle(NULL), MAKEINTRESOURCE(IMG_BITMAPQUIT)),0);
            SetMenuItemBitmaps(GetMenu(hwnd),MITEM_SAVEHtml,MF_BYCOMMAND, LoadBitmap(GetModuleHandle(NULL), MAKEINTRESOURCE(IMG_BITMAPSAVEHtml)),0);
            SetMenuItemBitmaps(GetMenu(hwnd),MITEM_Help,MF_BYCOMMAND, LoadBitmap(GetModuleHandle(NULL), MAKEINTRESOURCE(IMG_BITMAPAbout)),0);
            SetMenuItemBitmaps(GetMenu(hwnd),MITEM_ReSet,MF_BYCOMMAND, LoadBitmap(GetModuleHandle(NULL), MAKEINTRESOURCE(IMG_BITMAPReset)),0);
            SetMenuItemBitmaps(GetMenu(hwnd),MITEM_CHECK,MF_BYCOMMAND, LoadBitmap(GetModuleHandle(NULL), MAKEINTRESOURCE(IMG_BITMAPCheck)),0);
            SetMenuItemBitmaps(GetMenu(hwnd),MITEM_ADDone,MF_BYCOMMAND, LoadBitmap(GetModuleHandle(NULL), MAKEINTRESOURCE(IMG_BITMAPaddone)),0);
            SetMenuItemBitmaps(GetMenu(hwnd),MITEM_Close,MF_BYCOMMAND, LoadBitmap(GetModuleHandle(NULL), MAKEINTRESOURCE(IMG_BITMAPClose)),0);
            SetMenuItemBitmaps(GetMenu(hwnd),MITEM_Update,MF_BYCOMMAND, LoadBitmap(GetModuleHandle(NULL), MAKEINTRESOURCE(IMG_BITMAPUpdate)),0);
            SendMessage(hStatus,SB_SETBKCOLOR,(WPARAM)0,(LPARAM)0x00CCCCCC);
            int hh[4]={160,360,-1};
            SendMessage(hStatus,SB_SETPARTS,(WPARAM)2,(LPARAM)&hh);
            SendMessage(hStatus,SB_SETTEXT,(WPARAM)0,(LPARAM)"Game Not Started");
            txtbutn("",GetModuleHandle(NULL),hwnd,"bton",0,6,WinRect.bottom-80,40,24,563);
            if(File!=NULL)
            {
                        x=0;
                        while(x<9)
                        {
                            y=0;
                            while(y<9)
                            {
                                SDOK_NUMBERS[x][y]=fgetc(File);
                                if(SDOK_NUMBERS[x][y]>9 || SDOK_NUMBERS[x][y]<0)
                                {
                                    goto AnError;
                                }
                                y++;
                            }
                            x++;
                        }
                        x=0;
                        while(x<9)
                        {
                            y=0;
                            while(y<9)
                            {
                                SDOK_NUMBERS2[x][y]=fgetc(File);
                                if(SDOK_NUMBERS2[x][y]>9 || SDOK_NUMBERS2[x][y]<0)
                                {
                                    goto AnError;
                                }
                                y++;
                            }
                            x++;
                        }
                        x=0;
                        while(x<9)
                        {
                            y=0;
                            while(y<9)
                            {
                                SDOK_NUMBERS3[x][y]=fgetc(File);
                                if(SDOK_NUMBERS3[x][y]>9 || SDOK_NUMBERS3[x][y]<0)
                                {
                                        goto AnError;
                                }
                                y++;
                            }
                            x++;
                        }
                        fscanf(File,"%d %d %d",&HelpingOutCounter,&Level,&Time);
                        if(HelpingOutCounter>5 || ( Level>5 || Level<3))
                        {
                            goto AnError;
                        }
                        if( HelpingOutCounter==5)
                        {
                                SendMessage(ToolbarHwnd,TB_ENABLEBUTTON,(WPARAM)BTN_ADDone,(LPARAM)FALSE);
                                EnableMenuItem(GetMenu(hwnd),MITEM_ADDone,MF_DISABLED|MF_GRAYED);
                        }
                        GameStart=1;
                        fclose(File);
                        SetZero(NULL);
                        SendMessage(hwnd,WM_COMMAND,Mstr,0);
                         break;
                         AnError:
                            MessageBox(hwnd,"Unvalide File,Please Don't try to change the content of file",0,0);
                            GameStart=0;
                            fclose(File);
            }
            }
        break;
        case WM_CTLCOLOREDIT:
        {
            SetTextColor((HDC)wParam,RGB(255,0,0));
            SetBkMode((HDC)wParam,1);
            return (LONG)CreateSolidBrush(RGB(0,225,0));
        }
        break;
        case  WM_KEYDOWN :
            switch(wParam)
            {
                case VK_F1:
                    SendMessage(hwnd,WM_COMMAND,(WPARAM)MITEM_Help,(LPARAM)0);
                break;
                case VK_F2:
                printf("%d",GetKeyState(VK_F2));
                    PostQuitMessage(0);
                break;
                case VK_F3:
                     SendMessage(hwnd,WM_COMMAND,(WPARAM)MITEM_NewGame,(LPARAM)0);
                break;
                case VK_F6:
                     SendMessage(hwnd,WM_COMMAND,(WPARAM)MITEM_Update,(LPARAM)0);
                break;
            }
        break;
        case WM_SIZE:
            MoveWindow(ToolbarHwnd, 0, 0, LOWORD(lParam),HIWORD(lParam), TRUE);
            MoveWindow(hStatus, 0, 0, LOWORD(lParam),HIWORD(lParam), TRUE);
        break;
        case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC dc=BeginPaint(hwnd,&ps);
            hBarRect(dc,480,380,0x00CCCCCC);
            x=0;
            int X1[9];
            while(x<9)
            {
                X1[x]=0;
                x++;
            }
            x=0;
            while(x<9)
            {
                y=0;
                while(y<9)
                {
                    if(SDOK_NUMBERS2[x][y]>0  && SDOK_NUMBERS2[x][y]<10)
                            X1[SDOK_NUMBERS2[x][y]-1]++;
                    y++;
                }
                x++;
            }
            hbar(X1[0],dc,500,400,0x7777AA,1);
            hbar(X1[1],dc,520,400,0x00DDFF,2);
            hbar(X1[2],dc,540,400,0x22CC00,3);
            hbar(X1[3],dc,560,400,0x44BBFF,4);
            hbar(X1[4],dc,580,400,0x66FF00,5);
            hbar(X1[5],dc,600,400,0xAA88FF,6);
            hbar(X1[6],dc,620,400,0xAACC00,7);
            hbar(X1[7],dc,640,400,0xBBDDFF,8);
            hbar(X1[8],dc,660,400,0x00FF00,9);
            DeleteDC(dc);
            EndPaint(hwnd,&ps);
        }
        break;
        default:
            return DefWindowProc (hwnd, message, wParam, lParam);
    }
    return 0;
}
